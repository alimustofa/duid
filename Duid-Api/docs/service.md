# Add Service

Endpoint:
```sh
POST https://localhost:4003/api/v1/service?access_token=ACCESS_TOKEN
```

Request body example:
```json
{
    "name": "service 1",
    "harga": 12000
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Service added"
  },
  "data": {
    "__v": 0,
    "createdTime": "2016-05-26T19:05:48.000Z",
    "updatedTime": "2016-05-26T19:05:48.000Z",
    "name": "service1",
    "harga": 12000,
    "_id": "5746edb7cd2365083317b94c"
  }
}
```

# Get Service

Endpoint:
```sh
GET https://localhost:4003/api/v1/service?access_token=ACCESS_TOKEN&orderBy=columnName&orderType=orderType
```
_orderBy_ : Default by name
_orderType_ (asc/desc) : Default by asc

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Services list"
  },
  "data": [
    {
      "_id": "57456c4ed585b7f032605ca7",
      "createdTime": "2016-05-25T16:05:27.000Z",
      "updatedTime": "2016-05-25T16:05:27.000Z",
      "name": "Cuci2",
      "harga": 15000,
      "__v": 0
    },
    {
      "_id": "5746edb7cd2365083317b94c",
      "createdTime": "2016-05-26T19:05:48.000Z",
      "updatedTime": "2016-05-26T19:05:48.000Z",
      "name": "service1",
      "harga": 12000,
      "__v": 0
    }
  ]
}
```

# Update Service

_UpdateAble Fields_ : name, harga

Endpoint:
```sh
PUT https://localhost:4003/api/v1/service/:id?access_token=ACCESS_TOKEN
```

Request body example:
```json
{
    "name": "service2",
    "harga": 13000
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Service updated"
  },
  "data": {
    "_id": "5746edb7cd2365083317b94c",
    "createdTime": "2016-05-26T19:05:48.000Z",
    "updatedTime": "2016-05-26T19:05:14.000Z",
    "name": "service2",
    "harga": 13000,
    "__v": 0
  }
}
```

# Delete Service

Endpoint:
```sh
DELETE https://localhost:4003/api/v1/service/:id?access_token=ACCESS_TOKEN
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "OK"
  },
  "data": "Service removed"
}
```


