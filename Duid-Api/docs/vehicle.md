# Add Vehicle

Endpoint:
```sh
POST https://localhost:4003/api/v1/vehicle?access_token=ACCESS_TOKEN
```

Request body example:
```json
{
    "name": "vehicle 1",
    "harga": 12000
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Vehicle added"
  },
  "data": {
    "__v": 0,
    "createdTime": "2016-05-26T19:05:48.000Z",
    "updatedTime": "2016-05-26T19:05:48.000Z",
    "name": "vehicle 1",
    "harga": 12000,
    "_id": "5746edb7cd2365083317b94c"
  }
}
```

# Get Vehicle

Endpoint:
```sh
GET https://localhost:4003/api/v1/vehicle?access_token=ACCESS_TOKEN&orderBy=columnName&orderType=orderType
```
_orderBy_ : Default by name
_orderType_ (asc/desc) : Default by asc

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Vehicle list"
  },
  "data": [
    {
      "_id": "57456c4ed585b7f032605ca7",
      "createdTime": "2016-05-25T16:05:27.000Z",
      "updatedTime": "2016-05-25T16:05:27.000Z",
      "name": "vehicle 1",
      "harga": 12000,
      "__v": 0
    },
    {
      "_id": "5746edb7cd2365083317b94c",
      "createdTime": "2016-05-26T19:05:48.000Z",
      "updatedTime": "2016-05-26T19:05:48.000Z",
      "name": "vehicle 2",
      "harga": 13000,
      "__v": 0
    }
  ]
}
```

# Update Vehicle

_UpdateAble Fields_ : name, harga

Endpoint:
```sh
PUT https://localhost:4003/api/v1/vehicle/:id?access_token=ACCESS_TOKEN
```

Request body example:
```json
{
    "name": "vehicle 1 update",
    "harga": 15000
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Vehicle updated"
  },
  "data": {
    "_id": "5746edb7cd2365083317b94c",
    "createdTime": "2016-05-26T19:05:48.000Z",
    "updatedTime": "2016-05-26T19:05:14.000Z",
    "name": "vehicle 1 update",
    "harga": 15000,
    "__v": 0
  }
}
```

# Delete Vehicle

Endpoint:
```sh
DELETE https://localhost:4003/api/v1/vehicle/:id?access_token=ACCESS_TOKEN
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "OK"
  },
  "data": "Vehicle removed"
}
```


