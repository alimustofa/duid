# Add Absen

Endpoint:
```sh
POST localhost:4003/api/v1/absen?access_token=ACCESS_TOKEN
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Absen added"
  },
  "data": {
    "__v": 0,
    "createdTime": "2016-05-30T17:05:32.000Z",
    "updatedTime": "2016-05-30T17:05:32.000Z",
    "userId": "57448c186fc3a9c4266ae67c",
    "_id": "574c189f885fab141fd24d29"
  }
}
```

# Get Absen

Endpoint:
```sh
GET localhost:4003/api/v1/absen?access_token=ACCESS_TOKEN
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "User absen"
  },
  "data": [
    {
      "_id": "57469a8b53f5d3b010114cdb",
      "createdTime": "2016-05-26T13:05:57.000Z",
      "updatedTime": "2016-05-26T13:05:57.000Z",
      "userId": "57448c186fc3a9c4266ae67c",
      "__v": 0
    },
    {
      "_id": "57469aa353f5d3b010114cdc",
      "createdTime": "2016-05-26T13:05:11.000Z",
      "updatedTime": "2016-05-26T13:05:11.000Z",
      "userId": "57448c186fc3a9c4266ae67c",
      "__v": 0
    },
  ]
}
```

# Get Last Absen

Endpoint:
```sh
GET localhost:4003/api/v1/absen?last=yes&access_token=ACCESS_TOKEN
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "User absen"
  },
  "data": [
    {
      "_id": "574c189f885fab141fd24d29",
      "createdTime": "2016-05-30T17:05:32.000Z",
      "updatedTime": "2016-05-30T17:05:32.000Z",
      "userId": "57448c186fc3a9c4266ae67c",
      "__v": 0
    }
  ]
}
```

# Get All User Absen

Endpoint:
```sh
GET localhost:4003/api/v1/absen/allList?access_token=ACCESS_TOKEN
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "User absen"
  },
  "data": [
    {
      "_id": "57469a8b53f5d3b010114cdb",
      "createdTime": "2016-05-26T13:05:57.000Z",
      "updatedTime": "2016-05-26T13:05:57.000Z",
      "userId": "57448c186fc3a9c4266ae67c",
      "__v": 0
    },
    {
      "_id": "57469aa353f5d3b010114cdc",
      "createdTime": "2016-05-26T13:05:11.000Z",
      "updatedTime": "2016-05-26T13:05:11.000Z",
      "userId": "57448c186fc3a9c4266ae67c",
      "__v": 0
    },
  ]
}
```

