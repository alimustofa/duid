# Sign Up

Endpoint:
```sh
POST https://localhost:4003/api/v1/signup
```

Request body example:
```json
{
    "email": "alimustofa1@gmail.com",
    "password": "alimus",
    "phone": "081328989878",
    "fullname": "alimus"
}
```

Error response body example, when email is exist:
```
{
  "meta": {
    "code": 422,
    "message": "Email already exist"
  }
}
```

Successful response body example:
```
{
  "meta": {
    "code": 201,
    "message": "User created"
  },
  "data": {
    "__v": 0,
    "createdTime": "2016-05-26T19:05:54.000Z",
    "updatedTime": "2016-05-26T19:05:54.000Z",
    "email": "alimust@gmail.com",
    "phone": "081328989878",
    "_id": "5746ea68cd2365083317b94b",
    "active": false,
    "role": [
      "user"
    ],
    "fullname": "alimus"
  }
}
```

# User Profile

Endpoint:
```sh
GET https://localhost:4003/api/v1/user?access_token=ACCESS_TOKEN
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "OK"
  },
  "data": {
    "_id": "57448c186fc3a9c4266ae67c",
    "email": "alimustofa1@gmail.com",
    "phone": "+985786786",
    "__v": 0,
    "createdTime": "2016-05-26T13:05:34.000Z",
    "updatedTime": "2016-05-26T14:05:50.000Z",
    "active": true,
    "role": [
      "user"
    ],
    "fullname": "Akun test"
  }
}
```

# Update Profile

_UpdateAble Fields_ : phone, fullname, position

Endpoint:
```sh
PUT https://localhost:4003/api/v1/user?access_token=ACCESS_TOKEN
```

Request body example:
```json
{
    "phone": "087876765654",
    "fullname": "UserName"
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Profile updated"
  },
  "data": {
    "_id": "57448c186fc3a9c4266ae67c",
    "email": "alimustofa1@gmail.com",
    "phone": "087876765654",
    "__v": 0,
    "createdTime": "2016-05-26T13:05:34.000Z",
    "updatedTime": "2016-05-26T14:05:50.000Z",
    "active": true,
    "role": [
      "user"
    ],
    "fullname": "UserName"
  }
}
```

# Update Password

Endpoint:
```sh
PUT https://localhost:4003/api/v1/user/password?access_token=ACCESS_TOKEN
```

Request body example:
```json
{
    "password": "090",
    "confirmPassword": "090"
}
```

Error response, when _password_ & _confirmPassword_ not match
```
{
  "meta": {
    "code": 500,
    "message": "Password not match"
  }
}
```

Successful response body example:
```
{
  "meta": {
    "code": 200,
    "message": "Password updated"
  }
}
```


