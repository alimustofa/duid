This API uses OAuth2 Authorization Framework and follows the [standards](https://tools.ietf.org/html/rfc6749).

# Sign In

Endpoint:
```sh
POST https://localhost:4003/api/v1/signin
```

Request body example:
```json
{
    "grant_type": "password",
    "username": "alimustofa1@gmail.com",
    "password": "alimus",
    "client_id": "clientId",
    "client_secret": "clientSecret"
}
```

Successful response body example:
```
{
    "access_token": "128_bytes_length_token",
    "refresh_token": "128_bytes_length_refresh_token",
    "expiredin": "2016-11-26T10:42:34.411Z",
    "user": "userId",
    "token_type": "Bearer"
}
```

# Create Client

Endpoint:
```sh
POST https://localhost:4003/api/v1/oauth/client
```

Request body example:
```json
{
    "name": "ios",
    "description": "ios device"
}
```

Successful response body example:
```
{
  "meta": {
    "code": 201,
    "message": "Data Client Add"
  },
  "data": {
    "__v": 0,
    "createdTime": "2016-05-26T17:05:47.000Z",
    "updatedTime": "2016-05-26T17:05:47.000Z",
    "name": "ios",
    "_id": "5746d506cd2365083317b94a",
    "trusted": true,
    "scope": [
      "default_read",
      "default_write"
    ],
    "description": "ios device",
    "secret": "secretId"
  }
}
```

Error response is described on [this page](https://tools.ietf.org/html/rfc6749#section-5.2).
