'use strict';

const timestamp = require('./plugins/Timestamp');

const mongoose = require('mongoose');
const config = require('../../config/' + (process.env.NODE_ENV || ''));
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
const dataTables = require('mongoose-datatables');

const duidSchema = new Schema({
    title: {
        type: String,
    },
    jumlah: {
        type: Number,
    },
    // userId: {
    //     type: ObjectId,
    //     ref: 'User',
    // },
}, { collection: config.collection.name('duids'),
});

duidSchema.plugin(timestamp.useTimestamps);
duidSchema.plugin(dataTables, {
    totalKey: 'recordsTotal',
    dataKey: 'data',
});
module.exports = mongoose.model('Duid', duidSchema);
