/**
 * Created by faerulsalamun on 2/16/16.
 */

'use strict';

const mongoose = require('mongoose');
const crypto = require('crypto');
const _ = require('lodash');
const timestamp = require('./plugins/Timestamp');
const ObjectId = mongoose.Schema.Types.ObjectId;

const Schema = mongoose.Schema;
const OAuthRefreshTokenSchema = new Schema({

    token: {
        type: String,
        required: true,
    },

    client: {
        type: ObjectId,
        required: true,
        ref: 'OAuthClient',
    },

    user: {
        type: ObjectId,
        required: true,
        ref: 'User',
    },

}, { collection: 'oauth_refreshtoken' });

OAuthRefreshTokenSchema.plugin(timestamp.useTimestamps);
module.exports = mongoose.model('OAuthRefreshToken', OAuthRefreshTokenSchema);
