'use strict';

const passport = require('passport');
const _ = require('lodash');

exports.isAuthenticated = role => {

    return (req, res, next) => {

        //if (!req.isAuthenticated()) {
        //    req.session.returnTo = req.url;
        //    res.redirect('/signin');
        //    return;
        //}

        if (role && _.intersection(role, req.user.role).length === 0) {
            var err = new Error('Forbidden access');
            err.status = 401;
            return next(err);
        }

        next();
    };
};

exports.isAuthenticatedApi = () => {

    return (req, res, next) => {
        passport.authenticate(['accessToken'], { session: false })(req, res, next);
    };
};

exports.isAuthenticatedApiBasic = () => {

    return (req, res, next) => {
        passport.authenticate(['clientBasic'], { session: false })(req, res, next);
    };
};

exports.isAuthenticatedMulti = () => {

    return (req, res, next) => {
        passport.authenticate(['clientBasic', 'accessToken'], { session: false })(req, res, next);
    };
};
