/**
 * Created by faerulsalamun on 2/17/16.
 */

'use strict';

const crypto = require('crypto');

const passport = require('passport');
const Promise = require('bluebird');
const BearerStrategy = require('passport-http-bearer').Strategy;
const Async = require('../../services/Async');

// Models
const OAuthAccessToken = require('../../models/OAuthAccessToken');
const User = require('../../models/User');

/**
 * This strategy is used to authenticate users based on an access token (aka a bearer token).
 */

function wrap(genFunction) {
    const coroutine = Promise.coroutine(genFunction);

    return (accessToken, done) => {
        return coroutine(accessToken, done).catch(done);
    };
}

const handleAuth = wrap(function *(accessToken, done) {
    const accessTokenHash = crypto.createHash('sha1').update(accessToken).digest('hex');
    const token = yield OAuthAccessToken.findOne({ token: accessTokenHash });

    if (!token) return done(null, false);
    if (new Date() > token.expirationDate) {
        yield token.destroy().catch(console.error);
        return done(null);
    } else {
        const user = yield User.findOne({ _id: token.user });
        if (!user) return done(null, false);

        // no use of scopes for now
        var info = { scope: '*' };
        done(null, user, info);
    }
});

const strategy = new BearerStrategy(handleAuth);
passport.use('accessToken', strategy);

// passport.use('accessToken', new BearerStrategy(function(accessToken, done) {
//     const accessTokenHash = crypto.createHash('sha1').update(accessToken).digest('hex');
//     OAuthAccessToken.findOne({ tokenHash: accessTokenHash }, function(err, token) {
//         if (err) return done(err);
//         if (!token) return done(null, false);
//         if (new Date() > token.expirationDate) {
//             OAuthAccessToken.remove({ tokenHash: accessTokenHash }, function(err) {
//                 done(err);
//             });
//         } else {
//             User.findOne({ _id: token.user }, function(err, user) {
//                 if (err) return done(err);
//                 if (!user) return done(null, false);

//                 // no use of scopes for now
//                 var info = { scope: '*' };
//                 done(null, user, info);
//             });
//         }
//     });
// }));
