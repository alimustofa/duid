/**
 * Created by faerulsalamun on 6/25/16.
 */

'use strict';

const gcm = require('node-gcm');
const config = require('../../config/' + (process.env.NODE_ENV || ''));

// Services
const Async = require('../services/Async');
const Utils = require('../services/Utils');

module.exports = {
    sendNotif(registrationTokens, payload, callback) {
        payload.sound = 'default';
        payload.notification.icon = 'ic_launcher';
        payload.notification.title = 'Me Wash';

        let message = new gcm.Message(payload);
        let sender = new gcm.Sender(config.configFcm.key);

        sender.sendNoRetry(message, { registrationTokens: registrationTokens }, (err, response) => {
            if (err) console.error(err);
            else console.log(response);
        });
    },

    sendGrid(id, to, name, code, callback) {
        const sendgrid  = require('sendgrid')(config.sendgrid.secret);
        const url = config.configServer.urlWebApi + '/verify/' + code;
        const style = '<div style="width:300px; margin:auto; padding:15px;">' +
                        '<h1 align="center" style="color: #777777;">Hi ' + name + ', </h1>' +
                        '<h4 align="center" style="color: #777777;">Lets activate your account.</h4>' +
                        '<p align="center"><a href="' + url + '" style="text-decoration: none; padding:12px 45px; font-size:14px; background:#348eda; color:#fff; border-radius:3px;">ACTIVATE ACCOUNT</a></p>' +
                            '<p align="center"><h5 align="center" style="color: #777777;">&copy; Me Wash</h5></p>' +
                      '</div>';

        const email = new sendgrid.Email({
            from: 'mewash.noreplay@gmail.com',
            to: to,
            subject: 'Activate Account',
            html: style,
        });

        sendgrid.send(email, (err, json) => {
            if (err) return next(err);
            console.log(json);
        });
    },

    forgotPassword(to, data, callback) {
        const sendgrid  = require('sendgrid')(config.sendgrid.secret);
        const url = config.configServer.urlWebApi + '/forgot/' + data.forgotPassword;
        const style = '<div style="width:300px; margin:auto; padding:15px;">' +
                        '<h1 align="center" style="color: #777777;">Hi ' + data.fullname + ', </h1>' +
                        '<h4 align="center" style="color: #777777;">Click button below to reset your password.</h4>' +
                        '<p align="center"><a href="' + url + '" style="text-decoration: none; padding:12px 45px; font-size:14px; background:#348eda; color:#fff; border-radius:3px;">RESET PASSWORD</a></p>' +
                            '<p align="center"><h5 align="center" style="color: #777777;">&copy; Me Wash</h5></p>' +
                      '</div>';

        const email = new sendgrid.Email({
            from: 'mewash.noreplay@gmail.com',
            to: to,
            subject: 'Reset Password',
            html: style,
        });

        sendgrid.send(email, (err, json) => {
            if (err) return next(err);
            console.log(json);
        });
    },
};