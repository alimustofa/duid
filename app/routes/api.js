'use strict';

const express = require('express');
const router = express.Router();
const passport = require('passport');
const auth = require('../middlewares/authenticated');

// Controller
const OAuthController = require('../controllers/OAuthController');
const UserController = require('../controllers/UserController');
const NotificationController = require('../controllers/NotificationController');
const DuidController = require('../controllers/DuidController');

router.get('/', (req, res, next) => {
    res.ok('MeWash API Development Hook :)');
});

// User
// router.route('/user')
//     .put(auth.isAuthenticatedMulti(), UserController.updateProfile)
//     .get(auth.isAuthenticatedApi(), UserController.profile);
// router.put('/user/password', auth.isAuthenticatedApi(), UserController.updatePassword);
// router.put('/user/fcmToken', auth.isAuthenticatedApi(), UserController.updateToken);
// router.get('/logout', auth.isAuthenticatedApi(), UserController.logout);

// router.get('/tes', auth.isAuthenticatedApi(), auth.isAuthenticated(['user']), UserController.tes);

// Sign
// router.post('/signup', OAuthController.signup);
// router.post('/signin', OAuthController.getTokenDirectPassword);
// router.get('/verify/:code', UserController.verify);

// Forgot Password
// router.post('/forgotPassword', UserController.requestForgot);
// router.post('/submitPassword', UserController.submitForgot);
// router.get('/forgot/:code', UserController.forgotView);

// Create Client
// router.post('/oauth/client', OAuthController.createClientApi);

// Plat
router.route('/duid')
    .post(DuidController.add)
    .get(DuidController.list);

router.route('/duid/:id')
    .get(DuidController.detail)
    .put(DuidController.update)
    .delete(DuidController.remove);

// FCM
// router.post('/sendNotification', NotificationController.createTestPushNotification);

module.exports = router;
