/**
 * Created by faerulsalamun on 6/25/16.
 */

'use strict';

const gcm = require('node-gcm');
const config = require('../../config/' + (process.env.NODE_ENV || ''));

// Services
const Async = require('../services/Async');
const Utils = require('../services/Utils');

module.exports = {

    createTestPushNotification: Async.route(function *(req, res, next) {

        let registrationTokens = [req.body.token];

        let message = new gcm.Message();

        message.addData('message', 'Testing');

        let sender = new gcm.Sender(config.configFcm.key);

        sender.sendNoRetry(message, { registrationTokens: registrationTokens }, (err, response) => {
            if (err) console.error(err);
            else console.log(response);

            res.ok(response);
        });

    }),

};
