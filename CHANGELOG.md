23-05-2016
0.1.0
 - First Commit

29-05-2016
0.1.1
 - Change config production

30-05-2016
0.1.2
 - Add absen, order, setting docs
 - Fix bug lastAbsen list
 - Add filter order status
 - Disable hargaJarak in order
 - Fix update setting

31-05-2016
0.1.3
 - Add updateAble update user & order
 - Update docs

02-06-2016
0.1.4
 - Set active = true when sign up (temporary)

05-06-2016
0.1.5
 - Add address & addressNote in Order Model
 - Multiple filter status
 - Add required column (address & addressNote)

05-06-2016
0.1.6
 - Remove order feature
 
07-06-2016
0.2.0
 - Add mongose datatable example
 - Add api detail cabang
 - Add cors

12-06-2016
0.2.1
 - Add noPlat & inMerchant in Order add
 - Add dataTable in model & controller (order, service, user, vehicle)
 - Add rating controller

12-06-2016
0.2.2
 - Add service detail
 
17-06-2016
0.2.3
 - Add datatable
 - Handle order for cms
 - Add user controller for cms
 
20-06-2016
0.2.4
 - Handle userId when order
 - Change access permission
 
21-06-2016
0.2.5
 - Handle userId & washer when order

25-06-2016
0.3.0
 - Add fcm notification

27-06-2016
0.3.1
 - Disable cabang in cabangController
 - Add role in oauthController
 - Add cabang in resource order
 - Add cabang, if role 'admin' (UserAdminCont)
 - Update fcmToken
 - Fix bug edit pass
 - Add status cancelled (order)
 - Add fcmToken & cabang object model

28-06-2016
0.3.2
 - Remove address from cabang
 - Fix bug populate cabang - user

29-06-2016
0.3.3
 - Add notif when order
 - Verification email
 - Active = true, if admin & tukangCuci
 - Change url verification code

12-07-2016
0.3.4
 - Add graph order
 - Add admin list

14-07-2016
0.3.5
 - Add graph filter
 - Fix graph order

24-07-2016
0.3.6
 - Add column dt
 - Add type field
 - Handle order list by role

28-07-2016
0.3.7
 - Fix notif & condition washer when notAvailable

29-07-2016
0.3.8
 - Change userId to user - Order

04-08-2016
0.3.9
 - Add logout

28-08-2016
0.3.10
 - Add plat crud
 - Handle plat when order
 - Try promo feature

12-09-2016
0.3.11
 - Modify order, promo & userAdmin controller

13-09-2016 - 25-09-2016
0.3.12
 - Revisi

26-09-2016
0.3.13
 - Add leveling user
 
27-09-2016
0.3.14
 - Fix api promo (check, add, update, and datatable)
 - Change add data user for all role without activation
 - Fix some return code without variabel

30-09-2016
0.3.15
 - Change gaji model
 - Add waiting time

25-10-2016
0.3.16
 - Add object orderId for order (without test in local)

25-10-2016
0.3.17
 - Add object orderId for order (test in server)

26-10-2016
0.3.18
 - Fix search order

26-10-2016
0.3.19
 - Delete unused console.log :D

26-10-2016
0.3.20
 - Add object orderId for datatable order
 - Remove other fields for all datatable
 - Fix search order datatable
 
28-10-2016
0.3.21
 - Add populate userId for Order
 - Change status tukangCuci available when add user

28-10-2016
0.3.22
 - Fix bug change status tukangCuci available when add user
 
28-10-2016
0.3.23
 - Fix bug search datatable order
 
30-10-2016
 0.3.24
  - Test fix for bug search datatable order
  
30-10-2016
 0.3.25
  - Test(2) fix for bug search datatable order
  
30-10-2016
 0.3.26
  - Fix for bug search datatable order and models order
  
11-11-2016
 0.3.27
  - Add hargaPromo dtTable

